from django.conf.urls import url

from . import views

urlpatterns = [
    # url(r'^$', views.get_matches_summary),
    url(r'^$', views.get_match_data ,name="match"),
    url(r'^$', views.index, name='index'),
]