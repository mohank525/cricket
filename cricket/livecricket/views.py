from __future__ import print_function
import sys
import requests
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.shortcuts import render
import json


BASE_URL = "http://espncricinfo.com"
SUMMARY_URL = BASE_URL + "/netstorage/summary.json"
MATCH_URL = lambda match_url: BASE_URL + match_url[:-5] + ".json"

def get_match_data(request):
    try:
        # json_data = (requests.get(MATCH_URL_JSON(match_url), headers=REQUEST_PARAM, timeout=10)).json()
        json_data = requests.get("http://www.espncricinfo.com/netstorage/summary.json").json()
    except Exception as err:
    	print ('get_match_data: Exception: ', err, file=sys.stderr)
        return None
    # data = json.dump(json_data)

    data = json_data
    finaldata = []
    print(data)
    for key, value in data['matches'].iteritems() :
        print(value['url'])
        json = (requests.get(MATCH_URL(value['url']))).json()
        value['scoreboard'] = json
        value['key'] = key
        finaldata.append(value)
    

    #data = json.dumps(finaldata)    
    
    return render(request, 'index.html' ,
        {
        'data':finaldata
        })	
    # return HttpResponse("index.html")



def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

